#include <iostream>





class Matrix{
public:
    Matrix(int n, int m){
        m_n = n;
        m_m = m;
        m_mat = new int* [m_n];
        for(int i = 0; i < m_n; i++)
            m_mat[i] = new int[m_m];
    }

    Matrix(const Matrix& mat){

        std::cout<<"Copy Constructor"<<std::endl;

        m_n = mat.m_n;
        m_m = mat.m_m;

        m_mat = new int* [m_n];
        for(int i = 0; i < m_n; i++)
            m_mat[i] = new int[m_m];

        for(int i = 0; i < m_n; i++)
            for(int j = 0; j < m_m ; j++)
                m_mat[i][j] = mat.m_mat[i][j];
    }

    /*Matrix& operator-(const Matrix& mat){
        std::cout<<"operator+"<<std::endl;
        Matrix tmp(2,3);
        for(int i =0; i < m_n; i++)
            for(int j =0; j < m_m; j++)
                tmp.m_mat[i][j] = m_mat[i][j] - mat.m_mat[i][j] ;
        return tmp;
    }*/

    Matrix& operator=(const Matrix& mat){
        std::cout<<"operator="<<std::endl;
        m_n = mat.m_n;
        m_m = mat.m_m;

        for(int i = 0; i < m_n; i++)
            for(int j = 0; j < m_m ; j++)
                m_mat[i][j] = mat.m_mat[i][j];
        return *this;
    }

    Matrix& trans(const Matrix& mat){

        std::cout<<"Транспонированная матрица:"<<std::endl;
        Matrix tmp(m_n, m_m);

        for (int i = 0; i < m_n; i++){
            for (int j = 0; j < m_m; j++){
                tmp.m_mat[i][j] = mat.m_mat[j][i];
                std::cout<<tmp.m_mat[i][j]<<" ";
            }
            std::cout<<std::endl;
        }
        return tmp;
    }

    Matrix& obratmatrix(const Matrix& mat) {


        if(m_n > 3 || m_m > 3){
            std::cout<<"Обратная матрица: Операция не поддерживается"<<std::endl;
        }
        else{
            std::cout << "Обратная матрица: "<<std::endl;
            //Транспортировка
            Matrix tmp(m_n, m_m);
            for (int i = 0; i < m_n; i++)
                for (int j = 0; j < m_m; j++)
                    tmp.m_mat[i][j] = mat.m_mat[j][i];

            //Определитель
            int det = 0;
            if( m_n == 2 && m_m == 2)
                det = mat.m_mat[0][0] * mat.m_mat[1][1] - mat.m_mat[0][1] * mat.m_mat[1][0];
            else if( m_n == 3 && m_m == 3){
                int c,d;
                c = mat.m_mat[0][0] * mat.m_mat[1][1] * mat.m_mat[2][2] + mat.m_mat[0][1] * mat.m_mat[1][2] * mat.m_mat[2][0] + mat.m_mat[0][2] * mat.m_mat[1][0] * mat.m_mat[2][1];
                d = mat.m_mat[0][2] * mat.m_mat[1][1] * mat.m_mat[2][1] + mat.m_mat[0][0] * mat.m_mat[1][2] * mat.m_mat[2][1] + mat.m_mat[0][1] * mat.m_mat[1][0] * mat.m_mat[2][2];
                det = c - d;
            }

            for (int i = 0; i < m_n; i++){
                for (int j = 0; j < m_m; j++){
                    mat.m_mat[i][j] = tmp.m_mat[i][j] / det;
                    std::cout<<mat.m_mat[i][j]<<" ";
                }
                std::cout<<std::endl;
            }
        }

    }


    int determinant(const Matrix& mat){

        std::cout << "Определитель: ";
        int det = 0;
        if( m_n == 2 && m_m == 2){
            det = mat.m_mat[0][0] * mat.m_mat[1][1] - mat.m_mat[0][1] * mat.m_mat[1][0];
            return det;
        }

        else if( m_n == 3 && m_m == 3){
            int c,d;
            c = mat.m_mat[0][0] * mat.m_mat[1][1] * mat.m_mat[2][2] + mat.m_mat[0][1] * mat.m_mat[1][2] * mat.m_mat[2][0] + mat.m_mat[0][2] * mat.m_mat[1][0] * mat.m_mat[2][1];
            d = mat.m_mat[0][2] * mat.m_mat[1][1] * mat.m_mat[2][1] + mat.m_mat[0][0] * mat.m_mat[1][2] * mat.m_mat[2][1] + mat.m_mat[0][1] * mat.m_mat[1][0] * mat.m_mat[2][2];
            det = c - d;
            return det;
        }
        else if( m_n > 3 && m_m > 3){
            std::cout<<"Операция не поддерживается";
            std::cout<<std::endl;
            return 0;
        }
    }




    ~Matrix(){
        for (int i = 0; i < m_n; i++)
            delete[] m_mat[i];
        delete m_mat;

    }

    friend std::istream& operator>>(std::istream& in, Matrix& mat);
    friend std::ostream& operator<<(std::ostream& out, const Matrix& mat);


private:
    int** m_mat;
    int m_n, m_m;

};

std::istream& operator>>(std::istream& in, Matrix& mat){
    for(int i = 0; i < mat.m_n; i++)
        for(int j = 0; j < mat.m_m; j++)
            in >> mat.m_mat[i][j];
    return in;
}


std::ostream& operator<<(std::ostream& out, const Matrix& mat){
    for(int i = 0; i < mat.m_n; i++){
        for(int j = 0; j < mat.m_m; j++)
            out << mat.m_mat[i][j]<<" ";
        out<<std::endl;
    }
    return out;
}


int main() {

    int a , b;
    std::cout<<"Размеры матрицы "<<std::endl;
    std::cout<<"Количество строк: ";
    std::cin>>a;
    std::cout<<std::endl;
    std::cout<<"Количество столбцов: ";
    std::cin>>b;
    std::cout<<std::endl;

    Matrix A(a,b);
    std::cout<<"Матрица: "<<std::endl;
    std::cin >> A;


    std::cout<<A.trans(A);//Транспортировка
    std::cout<<A.determinant(A);//Определитель
    std::cout<<std::endl;
    std::cout<<A.obratmatrix(A);//Обратная матрица
    std::cout<<std::endl;


    return 0;
}
